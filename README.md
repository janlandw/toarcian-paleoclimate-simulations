# Toarcian Paleoclimate Simulations

The 180Ma_* and 185Ma_* directories contain CLIMBER-X model output from equilibrium paleoclimate simulations at various atmospheric pCO2 levels (300, 400, 500, 750, 1250, 1500 ppm), using paleogeographic reconstructions from Kocsis and Scotese 2020 (https://doi.org/10.1016/j.earscirev.2020.103463).

Currently, only NetCDF files containing the simulated top-layer ocean temperatures are included.

The Jupyter Notebook Toarcian_forCarl.ipynb provides an overview of these data.

The Jupyter Notebook Toarcian_description.ipynb discusses differences to initial results from 01/2022 resulting from changes in the model during its continued development. The data included in this repository are now based on the most current model version, which is close to that used for the accepted model description paper (Willeit et al. 2022, https://doi.org/10.5194/gmd-2022-56).
