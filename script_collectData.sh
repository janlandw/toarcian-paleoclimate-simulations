for ii in ../climberx_output/18[0,5]Ma_*;
do runname=$(basename "$ii");
#do echo $ii;
echo $runname;
mkdir $runname;
cp -a $ii/ocn.nc ${runname}/;
cd $runname;
#ncks -O -v t -d time,9 -d lev,0 ocn.nc ocn_varsel.nc;
ncks -O -v t -d lev,0 ocn.nc ocn_varsel.nc;
rm -rf ocn.nc;
cd ..;
done

# dir_Valdes='../python3/Data_fromOthers/ClimateModel_Valdes2021/'
# for ii in 'data_scotese_spinupa/texYl' 'data_scotese_spinupa/texYk' 'data_scotese_2/texPl1' 'data_scotese_2/texPk1';
# #do runname=$(basename "$ii");
# do runname=$(basename "$ii");
# #mkdir $runname;
# echo ${dir_Valdes}${ii};
# cp -a ${dir_Valdes}${ii}/*o.pfcl_allmonths.nc ${runname}/;
# #mkdir $runname;
# done

# for ii in 18[0,5]Ma_*;
# do echo $ii;
# cd $ii;
# ncks -O -v t -d time,9 -d lev,0 ocn.nc ocn_varsel.nc;
# cd ..;
# done;
